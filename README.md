# bitcoind

Simple docker image for running bitcoind


## Create a directory where your bitcoin data will be stored.
$ mkdir /home/youruser/bitcoin_data

## Configuration

A `bitcoin.conf` file should be placed in the mounted data directory.

## Run

$ docker run -v /path/to/bitcoin:/bitcoin/.bitcoin --name bitcoin -d -p 8333:8333 --restart always  eambrose/bitcoind

$ docker logs -f bitcoind
